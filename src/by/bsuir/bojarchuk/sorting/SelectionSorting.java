package by.bsuir.bojarchuk.sorting;

/**
 * Created by Shymko_A on 04.04.2016.
 */
public class SelectionSorting extends Sorting {

    public SelectionSorting() {
        super();
    }

    @Override
    public void sort() {
        for (int min = 0; min < array.length - 1; min++) {
            int least = min;
            for (int j = min + 1; j < array.length; j++) {
                if (array[j] < array[least]) {
                    least = j;
                }
            }
            int tmp = array[min];
            array[min] = array[least];
            array[least] = tmp;
        }
        countMemory();
    }
}
