package by.bsuir.bojarchuk.sorting;

/**
 * Created by Shymko_A on 04.04.2016.
 */
public class BubbleSorting extends Sorting {

    public BubbleSorting(){
        super();
    }

    @Override
    public void sort() {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
        }
        memory = runtime.totalMemory() - runtime.freeMemory();
    }

}
