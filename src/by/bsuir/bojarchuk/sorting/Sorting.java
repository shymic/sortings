package by.bsuir.bojarchuk.sorting;

import java.util.List;
import java.util.Random;

/**
 * Created by Shymko_A on 04.04.2016.
 */
public abstract class Sorting {

    private static final int N = 1000;
    private Random rnd = new Random();

    public Runtime runtime = Runtime.getRuntime();

    protected int[] array;

    protected long time;
    protected long memory;

    public Sorting(){
        array = new int[N];
        for (int i = 0; i < array.length; ++i){
            array[i] = rnd.nextInt(N);
        }
    }

    public long getTime(){
        return time;
    }

    public long getMemory(){
        return memory;
    }

    public void countTime(){
        long startTime = System.nanoTime();
        sort();
        long endTime = System.nanoTime();
        this.time = endTime - startTime;
    }

    public void countMemory(){
        this. memory = runtime.totalMemory() - runtime.freeMemory();
    }

    public void printArray(){
        for( int i : array){
            System.out.print(i + " ");
        }
        System.out.println();
    }

    public abstract void sort();

}
