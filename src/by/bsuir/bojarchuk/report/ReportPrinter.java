package by.bsuir.bojarchuk.report;

import by.bsuir.bojarchuk.sorting.Sorting;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Shymko_A on 04.04.2016.
 */
public class ReportPrinter {
    private File file ;
    private PrintWriter pw;

    public void createReport(Sorting s) {
        try {
            file = new File("report_" + s.getClass() + ".csv");
            pw=new PrintWriter(file);
            s.countTime();
            pw.println("Time ; " + s.getTime());
            pw.println("Memory; " + s.getMemory());
            pw.flush();
            pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
