package by.bsuir.bojarchuk.test;


import by.bsuir.bojarchuk.report.ReportPrinter;
import by.bsuir.bojarchuk.sorting.BubbleSorting;
import by.bsuir.bojarchuk.sorting.QuickSorting;
import by.bsuir.bojarchuk.sorting.SelectionSorting;
import by.bsuir.bojarchuk.sorting.Sorting;

import java.util.GregorianCalendar;

/**
 * Created by Shymko_A on 04.04.2016.
 */
public class Test {
    private static ReportPrinter reportPrinter = new ReportPrinter();

    public static void main(String[] args) {

        BubbleSorting bs = new BubbleSorting();
        QuickSorting qs = new QuickSorting();
        SelectionSorting ss = new SelectionSorting();

        reportPrinter.createReport(bs);
        reportPrinter.createReport(qs);
        reportPrinter.createReport(ss);



    }
}
